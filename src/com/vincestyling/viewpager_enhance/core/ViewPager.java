/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.vincestyling.viewpager_enhance.core;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

/**
 * Layout manager that allows the user to flip left and right
 * through pages of data.  You supply an implementation of a
 * {@link PagerAdapter} to generate the pages that the view shows.
 *
 * <p>Note this class is currently under early design and
 * development.  The API will likely change in later updates of
 * the compatibility library, requiring changes to the source code
 * of apps when they are compiled against the newer version.</p>
 */
public class ViewPager extends BaseViewPager {
	public ViewPager(Context context) {
		this(context, null);
	}

	public ViewPager(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}

	public ViewPager(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	boolean mIsJumpThrough;

	@Override
	void setCurrentItemInternal(int item, boolean smoothScroll, boolean always, int velocity) {
		if (mAdapter == null || mAdapter.getCount() <= 0) {
			setScrollingCacheEnabled(false);
			return;
		}

		if (!always && mCurItem == item && getItemCount() != 0) {
			setScrollingCacheEnabled(false);
			return;
		}

		if (item < 0) {
			item = 0;
		} else if (item >= mAdapter.getCount()) {
			item = mAdapter.getCount() - 1;
		}

		final boolean dispatchSelected = mCurItem != item;
		mIsJumpThrough = Math.abs(mCurItem - item) > 1;
		int destX;

		if (mIsBeingDragged || mFakeDragging || !smoothScroll) {
			destX = (getWidth() + mPageMargin) * item;
			mCurItem = item;
			populate();
		} else {
			boolean isForward = mCurItem < item;

			mAdapter.startUpdate(this);

			ensureItem(item);

			ItemInfo curItem = mItems.get(item);
			mAdapter.setPrimaryItem(this, item, curItem != null ? curItem.object : null);

			mAdapter.finishUpdate(this);

			for (ItemInfo mItem : mItems) {
				if (mItem != null) {
					mItem.tmpPosition = mItem.position;
				}
			}

			int comingPos = isForward ? Math.min(mItems.size() - 1, mCurItem + 1) : Math.max(0, mCurItem - 1);
			ItemInfo comingItem = mItems.get(comingPos);

			if (comingItem != curItem) {
				if (comingItem == null || curItem == null) return;
				comingItem.tmpPosition = curItem.position;
				curItem.tmpPosition = comingItem.position;
			}

			destX = getScrollX() + (isForward ? (getWidth() + mPageMargin) : -(getWidth() + mPageMargin));

			mDisableTraversal = true;
			mCurItem = item;
		}

		if (smoothScroll) {
			smoothScrollTo(destX, 0, velocity);
			if (dispatchSelected && mOnPageChangeListener != null) {
				mOnPageChangeListener.onPageSelected(item);
			}
		} else {
			if (dispatchSelected && mOnPageChangeListener != null) {
				mOnPageChangeListener.onPageSelected(item);
			}
			completeScroll();
			scrollTo(destX, 0);
		}
	}

	@Override
	void populate() {
		if (mDisableTraversal || mAdapter == null || mAdapter.getCount() == 0) return;

		// Bail now if we are waiting to populate.  This is to hold off
		// on creating views from the time the user releases their finger to
		// fling to a new position until we have finished the scroll to
		// that position, avoiding glitches from happening at that point.
		if (mPopulatePending) {
			if (DEBUG) Log.i(TAG, "populate is pending, skipping for now...");
			return;
		}

		// Also, don't populate until we are attached to a window.  This is to
		// avoid trying to populate before we have restored our view hierarchy
		// state and conflicting with what is restored.
		if (getWindowToken() == null) {
			return;
		}

		mAdapter.startUpdate(this);

		final int pageLimit = mOffscreenPageLimit;
		final int startPos = Math.max(0, mCurItem - pageLimit);
		final int N = mAdapter.getCount();
		final int endPos = Math.min(N - 1, mCurItem + pageLimit);

		if (DEBUG) Log.v(TAG, "populating: startPos=" + startPos + " endPos=" + endPos);

		// Add and destroy pages in the existing list.
		for (int pos = 0; pos < mItems.size(); pos++) {
			if (pos < startPos || pos > endPos) {
				ItemInfo ii = mItems.get(pos);
				if (ii != null) {
					if (DEBUG) Log.i(TAG, "removing: @ " + pos);
					mAdapter.destroyItem(this, pos, ii.object);
					mItems.set(pos, null);
				}
			} else {
				if (DEBUG) Log.i(TAG, "determinating position: @ " + pos);
				ensureItem(pos);
			}
		}

		if (DEBUG) {
			Log.i(TAG, "Current page list:");
			for (ItemInfo mItem : mItems) {
				if (mItem != null) Log.i(TAG, "page " + mItem.position);
			}
		}

		ItemInfo curItem = mItems.get(mCurItem);
		mAdapter.setPrimaryItem(this, mCurItem, curItem != null ? curItem.object : null);

		mAdapter.finishUpdate(this);

		if (hasFocus()) {
			View currentFocused = findFocus();
			ItemInfo ii = currentFocused != null ? infoForAnyChild(currentFocused) : null;
			if (ii == null || ii.position != mCurItem) {
				for (int i = 0; i < getChildCount(); i++) {
					View child = getChildAt(i);
					ii = infoForChild(child);
					if (ii != null && ii.position == mCurItem) {
						if (child.requestFocus(FOCUS_FORWARD)) {
							break;
						}
					}
				}
			}
		}
	}

	/**
	 * Like {@link View#scrollBy}, but scroll smoothly instead of immediately.
	 *
	 * @param x the number of pixels to scroll by on the X axis
	 * @param y the number of pixels to scroll by on the Y axis
	 * @param velocity the velocity associated with a fling, if applicable. (0 otherwise)
	 */
	void smoothScrollTo(int x, int y, int velocity) {
		if (getChildCount() == 0) {
			// Nothing to do.
			setScrollingCacheEnabled(false);
			return;
		}
		int sx = getScrollX();
		int sy = getScrollY();
		int dx = x - sx;
		int dy = y - sy;
		if (dx == 0 && dy == 0) {
			completeScroll();
			setScrollState(SCROLL_STATE_IDLE);
			return;
		}

		setScrollingCacheEnabled(true);
		mScrolling = true;
		setScrollState(SCROLL_STATE_SETTLING);

		final float pageDelta = (float) Math.abs(dx) / (getWidth() + mPageMargin);
		int duration = (int) (pageDelta * 100);

		velocity = Math.abs(velocity);
		if (velocity > 0) {
			duration += (duration / (velocity / mBaseLineFlingVelocity)) * mFlingVelocityInfluence;
		} else {
			duration += 100;
		}
		duration = Math.min(duration, MAX_SETTLE_DURATION);

		mScroller.startScroll(sx, sy, dx, dy, duration);
		invalidate();
	}

	@Override
	public void computeScroll() {
		if (DEBUG) Log.i(TAG, "computeScroll: finished=" + mScroller.isFinished());
		if (!mScroller.isFinished()) {
			if (mScroller.computeScrollOffset()) {
				if (DEBUG) Log.i(TAG, "computeScroll: still scrolling");
				int oldX = getScrollX();
				int oldY = getScrollY();
				int x = mScroller.getCurrX();
				int y = mScroller.getCurrY();

				if (oldX != x || oldY != y) {
					scrollTo(x, y);
				}

				if (mOnPageChangeListener != null) {
					final int widthWithMargin = getWidth() + mPageMargin;
					final int offsetPixels = x % widthWithMargin;
					if (mIsJumpThrough) {
						int startPos = mScroller.getStartX() / widthWithMargin;
						int totalScrollPixels = mScroller.getFinalX() - mScroller.getStartX();
						float pixelsInEachItem = totalScrollPixels / (mCurItem - startPos);

						float convertedOffset = Math.abs(x - mScroller.getStartX()) / pixelsInEachItem;
						convertedOffset = startPos > mCurItem ?
								startPos - convertedOffset : startPos + convertedOffset;
						int position = (int) convertedOffset;
						float offset = convertedOffset - position;

						mOnPageChangeListener.onPageScrolled(position, offset, offsetPixels);
					} else {
						int position = x / widthWithMargin;
						float offset = (float) offsetPixels / widthWithMargin;
						mOnPageChangeListener.onPageScrolled(position, offset, offsetPixels);
					}
				}

				// Keep on drawing until the animation has finished.
				invalidate();
				return;
			}
		}

		// Done with scroll, clean up state.
		completeScroll();
	}

	@Override
	void completeScroll() {
		if (mAdapter == null || mAdapter.getCount() == 0) return;

		boolean needPopulate = mScrolling;
		if (needPopulate) {
			// Done with scroll, no longer want to cache view drawing.
			setScrollingCacheEnabled(false);
			mScroller.abortAnimation();
			int oldX = getScrollX();
			int oldY = getScrollY();
			int x = mScroller.getCurrX();
			int y = mScroller.getCurrY();
			if (oldX != x || oldY != y) {
				scrollTo(x, y);
			}
			setScrollState(SCROLL_STATE_IDLE);
		}

		mDisableTraversal = false;
		mPopulatePending = false;
		mScrolling = false;

		if (needPopulate) {
			populate();
		} else if (!mIsBeingDragged && !mFakeDragging) {
			ItemInfo item = mItems.get(mCurItem);
			if (item != null) {
				View curView = ((Fragment) item.object).getView();
				scrollTo(curView.getLeft(), curView.getTop());
			}
		}
	}
}
