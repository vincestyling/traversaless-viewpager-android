package com.vincestyling.viewpager_enhance.frags;

import android.graphics.Color;

public class SearchView extends BookshelfView {
	@Override
	protected int getBackgroundColor() {
		return 0xff778899;
	}

	@Override
	protected int getTextColor() {
		return Color.RED;
	}
}
